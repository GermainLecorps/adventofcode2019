﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2019.Business
{
    public class Day012019
    {
        private List<string> _Input { get; set; }

        public Day012019(List<string> input)
        {
            _Input = input;
        }

        public string Part1()
        {
            return _Input.Select(line =>
            {
                var num = float.Parse(line);
                return Math.Truncate(num / 3.0) - 2;
            }).Sum().ToString();
        }

        public string Part2()
        {
            return _Input.Select(line =>
            {
                double total = 0;
                var num = double.Parse(line);
                while (true)
                {
                    var compute = Math.Truncate(num / 3.0) - 2;
                    if (compute <= 0)
                    {
                        break;
                    }
                    total += compute;
                    num = compute;
                }
                return total;
            }).Sum().ToString();
        }
    }
}
