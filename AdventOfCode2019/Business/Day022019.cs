﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2019.Business
{
    public class Day022019
    {
        private List<string> _Input { get; set; }

        public Day022019(List<string> input)
        {
            _Input = input;
        }

        public string Part1()
        {
            return ModifyArray(12, 2).ToString();
        }

        private void Multiply(List<int> array, int idx)
        {
            var multiply = array[array[idx + 1]] * array[array[idx + 2]];
            array[array[idx + 3]] = multiply;
        }

        private void Adds(List<int> array, int idx)
        {
            var add = array[array[idx + 1]] + array[array[idx + 2]];
            array[array[idx + 3]] = add;
        }

        public string Part2()
        {
            var addr0 = 0;
            for (int noun = 0; noun < 100; noun++)
            {
                for (int verb = 0; verb < 100; verb++)
                {
                    addr0 = ModifyArray(noun, verb);
                    if (addr0 == 19690720)
                    {
                        return (100 * noun + verb).ToString();
                    }
                }
            }
            return "Result could not be found";
        }


        private int ModifyArray(int noun, int verb)
        {
            var line = _Input[0];
            var array = line.Split(",").Select(val => Int32.Parse(val)).ToList();

            var condition = true;
            array[1] = noun;
            array[2] = verb;
            for (int idx = 0; condition; idx += 4)
            {
                switch (array[idx])
                {
                    case 1:
                        Adds(array, idx);
                        break;
                    case 2:
                        Multiply(array, idx);
                        break;
                    default:
                        condition = false;
                        break;
                }
            }
            return array[0];
        }
    }
}
