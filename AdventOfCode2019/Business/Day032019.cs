﻿using AdventOfCode2019.Extensions;
using AdventOfCode2019.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode2019.Business
{
    public class Day032019
    {
        private List<string> _Input { get; set; }
        private int Start { get; set; }
        private Dictionary<Coordinates, string> ShortestPath { get; set; } = new Dictionary<Coordinates, string>();
        private Dictionary<Coordinates, int> Path { get; set; } = new Dictionary<Coordinates, int>();

        public Day032019(List<string> input)
        {
            _Input = input;
            Start = 50000;
        }

        public string Part1()
        {
            ModifyArray(0);
            ModifyArray(1);
            return Path.Where(obj => obj.Value == 2).Select(obj => ComputeManhattan(obj.Key.X, obj.Key.Y)).Min().ToString();
        }

        private void ModifyArray(int val)
        {
            var instructions = _Input[val].Split(",");
            var x = 0;
            var y = 0;

            instructions.ToList().ForEach(inst =>
            {
                var direction = inst.Substring(0, 1);
                var length = int.Parse(inst.Substring(1));
                var dist = 0;
                switch (direction)
                {
                    case "R":
                        dist = y + length;
                        while (y < dist)
                        {
                            if (Path.ContainsKey(new Coordinates(x, y)) && Path[new Coordinates(x, y)] == 0 && val == 1)
                            {
                                Path[new Coordinates(x, y)] = 2;
                            }
                            else
                            {
                                Path[new Coordinates(x, y)] = val;
                            }
                            y++;
                        }
                        break;
                    case "D":
                        dist = x - length;
                        while (x > dist)
                        {
                            if (Path.ContainsKey(new Coordinates(x, y)) && Path[new Coordinates(x, y)] == 0 && val == 1)
                            {
                                Path[new Coordinates(x, y)] = 2;
                            }
                            else
                            {
                                Path[new Coordinates(x, y)] = val;
                            }
                            x--;
                        }
                        break;
                    case "U":
                        dist = x + length;
                        while (x < dist)
                        {
                            if (Path.ContainsKey(new Coordinates(x, y)) && Path[new Coordinates(x, y)] == 0 && val == 1)
                            {
                                Path[new Coordinates(x, y)] = 2;
                            }
                            else
                            {
                                Path[new Coordinates(x, y)] = val;
                            }
                            x++;
                        }
                        break;
                    default:
                        dist = y - length;
                        while (y > dist)
                        {
                            if (Path.ContainsKey(new Coordinates(x, y)) && Path[new Coordinates(x, y)] == 0 && val == 1)
                            {
                                Path[new Coordinates(x, y)] = 2;
                            }
                            else
                            {
                                Path[new Coordinates(x, y)] = val;
                            }
                            y--;
                        }
                        break;
                }
            });
            Path[new Coordinates(0, 0)] = 99;
        }

        private void ComputePaths()
        {
            ModifyArray(0);
            ModifyArray(1);

            var intersections = Path.Where(obj => obj.Value == 2).ToDictionary(k => k.Key, k => k.Value);
            ComputeOnePath(0, intersections);
            ComputeOnePath(1, intersections);
        }

        private void ComputeOnePath(int val, Dictionary<Coordinates, int> intersections)
        {
            var instructions = _Input[val].Split(",");
            var x = 0;
            var y = 0;
            var count = 0;

            instructions.ToList().ForEach(inst =>
            {
                var direction = inst.Substring(0, 1);
                var length = int.Parse(inst.Substring(1));
                var dist = 0;
                switch (direction)
                {
                    case "R":
                        dist = y + length;
                        while (y < dist)
                        {
                            if (intersections.ContainsKey(new Coordinates(x, y)) && val == 0 && !ShortestPath.ContainsKey(new Coordinates(x, y)))
                            {
                                ShortestPath[new Coordinates(x, y)] = count.ToString();
                            }
                            else if (intersections.ContainsKey(new Coordinates(x, y)) && val == 1 && ShortestPath[new Coordinates(x, y)].Split(",").Count() == 1)
                            {
                                ShortestPath[new Coordinates(x, y)] = $"{ShortestPath[new Coordinates(x, y)]},{count.ToString()}";
                            }
                            y++;
                            count++;
                        }
                        break;
                    case "D":
                        dist = x - length;
                        while (x > dist)
                        {
                            if (intersections.ContainsKey(new Coordinates(x, y)) && val == 0 && !ShortestPath.ContainsKey(new Coordinates(x, y)))
                            {
                                ShortestPath[new Coordinates(x, y)] = count.ToString();
                            }
                            else if (intersections.ContainsKey(new Coordinates(x, y)) && val == 1 && ShortestPath[new Coordinates(x, y)].Split(",").Count() == 1)
                            {
                                ShortestPath[new Coordinates(x, y)] = $"{ShortestPath[new Coordinates(x, y)]},{count.ToString()}";
                            }
                            x--;
                            count++;
                        }
                        break;
                    case "U":
                        dist = x + length;
                        while (x < dist)
                        {
                            if (intersections.ContainsKey(new Coordinates(x, y)) && val == 0 && !ShortestPath.ContainsKey(new Coordinates(x, y)))
                            {
                                ShortestPath[new Coordinates(x, y)] = count.ToString();
                            }
                            else if (intersections.ContainsKey(new Coordinates(x, y)) && val == 1 && ShortestPath[new Coordinates(x, y)].Split(",").Count() == 1)
                            {
                                ShortestPath[new Coordinates(x, y)] = $"{ShortestPath[new Coordinates(x, y)]},{count.ToString()}";
                            }
                            x++;
                            count++;
                        }
                        break;
                    default:
                        dist = y - length;
                        while (y > dist)
                        {
                            if (intersections.ContainsKey(new Coordinates(x, y)) && val == 0 && !ShortestPath.ContainsKey(new Coordinates(x, y)))
                            {
                                ShortestPath[new Coordinates(x, y)] = count.ToString();
                            }
                            else if (intersections.ContainsKey(new Coordinates(x, y)) && val == 1 && ShortestPath[new Coordinates(x, y)].Split(",").Count() == 1)
                            {
                                ShortestPath[new Coordinates(x, y)] = $"{ShortestPath[new Coordinates(x, y)]},{count.ToString()}";
                            }
                            y--;
                            count++;
                        }
                        break;
                }
            });
        }

        private int ComputeManhattan(int x, int y)
        {
            return Math.Abs(x) + Math.Abs(y);
        }

        public string Part2()
        {
            ComputePaths();
            return ShortestPath.Select(couple => int.Parse(couple.Value.Split(",")[0]) + int.Parse(couple.Value.Split(",")[1])).Min().ToString();
        }
    }
}
