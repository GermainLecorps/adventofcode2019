import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AoC2019Component } from './ao-c2019.component';

describe('AoC2019Component', () => {
  let component: AoC2019Component;
  let fixture: ComponentFixture<AoC2019Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AoC2019Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AoC2019Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
