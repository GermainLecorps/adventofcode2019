import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComputeBothPartsComponent } from './compute-both-parts.component';

describe('ComputeBothPartsComponent', () => {
  let component: ComputeBothPartsComponent;
  let fixture: ComponentFixture<ComputeBothPartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComputeBothPartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComputeBothPartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
