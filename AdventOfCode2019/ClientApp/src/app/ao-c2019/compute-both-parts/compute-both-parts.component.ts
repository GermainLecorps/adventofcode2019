import { Component, OnInit, Input, Inject } from '@angular/core';
import { TwentyNineteenService } from 'src/app/api/services';
import { Utils } from 'src/app/helpers/utils';
import { DisplayMessageService } from 'src/app/services/displayMessage';


@Component({
  selector: 'app-compute-both-parts',
  templateUrl: './compute-both-parts.component.html',
  styleUrls: ['./compute-both-parts.component.css']
})
export class ComputeBothPartsComponent implements OnInit {

  @Input() day: number;
  input: string;
  result: string;

  constructor(private readonly _service: TwentyNineteenService, private readonly _msgService: DisplayMessageService) { }

  ngOnInit() {

  }

  part1() {
    if (Utils.isNullOrEmpty(this.input)) {
      this._msgService.openSnackBar({ action: 'Fermer', isError: true, message: 'Input must contain text', time: 5000 });
      return;
    }
    this._service.twentyNineteenPart1({ body: this.input.replace(/\r?\n/g, '\\n').split('\\n'), day: this.day }).subscribe(res => this.result = res);
  }

  part2() {
    if (Utils.isNullOrEmpty(this.input)) {
      this._msgService.openSnackBar({ action: 'Fermer', isError: true, message: 'Input must contain text', time: 5000 });
      return;
    }
    this._service.twentyNineteenPart2({ body: this.input.replace(/\r?\n/g, '\\n').split('\\n'), day: this.day }).subscribe(res => this.result = res);
  }
}
