/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';


@Injectable({
  providedIn: 'root',
})
export class TwentyNineteenService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation twentyNineteenPart1
   */
  static readonly TwentyNineteenPart1Path = '/part1/{day}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `twentyNineteenPart1()` instead.
   *
   * This method sends `application/json` and handles response body of type `application/json`
   */
  twentyNineteenPart1$Response(params: {
    day: number;

    body: Array<string>
  }): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, TwentyNineteenService.TwentyNineteenPart1Path, 'post');
    if (params) {

      rb.path('day', params.day);

      rb.body(params.body, 'application/json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `twentyNineteenPart1$Response()` instead.
   *
   * This method sends `application/json` and handles response body of type `application/json`
   */
  twentyNineteenPart1(params: {
    day: number;

    body: Array<string>
  }): Observable<string> {

    return this.twentyNineteenPart1$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

  /**
   * Path part for operation twentyNineteenPart2
   */
  static readonly TwentyNineteenPart2Path = '/part2/{day}';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `twentyNineteenPart2()` instead.
   *
   * This method sends `application/json` and handles response body of type `application/json`
   */
  twentyNineteenPart2$Response(params: {
    day: number;

    body: Array<string>
  }): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, TwentyNineteenService.TwentyNineteenPart2Path, 'post');
    if (params) {

      rb.path('day', params.day);

      rb.body(params.body, 'application/json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `twentyNineteenPart2$Response()` instead.
   *
   * This method sends `application/json` and handles response body of type `application/json`
   */
  twentyNineteenPart2(params: {
    day: number;

    body: Array<string>
  }): Observable<string> {

    return this.twentyNineteenPart2$Response(params).pipe(
      map((r: StrictHttpResponse<string>) => r.body as string)
    );
  }

}
