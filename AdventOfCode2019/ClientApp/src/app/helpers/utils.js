"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils = /** @class */ (function () {
    function Utils() {
    }
    /**
      * Utility method to check if a string is null or empty
      * @param str the string to check
      */
    Utils.isNullOrEmpty = function (str) {
        return str == null || str === undefined || (typeof str === "string" && str === '');
    };
    return Utils;
}());
exports.Utils = Utils;
//# sourceMappingURL=utils.js.map