import { Injectable } from "@angular/core";

export class Utils {

  /**
    * Utility method to check if a string is null or empty
    * @param str the string to check
    */
  static isNullOrEmpty(str: any): boolean {
    return str == null || str === undefined || (typeof str === "string" && str === '');
  }



}
