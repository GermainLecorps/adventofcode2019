export class SnackBarInfo {
  message: string;
  action: string;
  time: number;
  isError: boolean;
}
