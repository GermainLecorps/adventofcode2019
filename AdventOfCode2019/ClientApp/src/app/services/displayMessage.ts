import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { SnackBarInfo } from '../models/snackBarInfo';

@Injectable({
  providedIn: 'root'
})
export class DisplayMessageService {

  constructor(public errorBar: MatSnackBar,
    public infoBar: MatSnackBar,
    private readonly zone: NgZone) { }

  /**
 * Method to display a context message
 * @param message the message to display
 * @param action the action to display to close the message
 * @param time the duration of the message
 * @param type the type of snackbar to display (info or error)
 */
  openSnackBar(notif: SnackBarInfo) {
    this.zone.run(() => {
      if (notif.isError) {
        this.errorBar.open("⚠️  " + notif.message, notif.action, {
          duration: notif.time, panelClass: ['errorDialog']
        });
      } else {
        this.infoBar.open("ℹ️  " + notif.message, notif.action, {
          duration: notif.time, panelClass: ['infoDialog']
        });
      }
    });
  }
}
