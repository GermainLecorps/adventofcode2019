﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode2019.Business;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AdventOfCode2019.Controllers
{
    public class TwentyNineteenController : Controller
    {
        [HttpPost]
        [Route("part1/{day}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult Part1([FromBody]List<string> input, int day)
        {
            return Ok(Part1_Compute(input, day));
        }

        [HttpPost]
        [Route("part2/{day}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public IActionResult Part2([FromBody]List<string> input, int day)
        {
            return Ok(Part2_Compute(input, day));
        }


        private string Part1_Compute(List<string> input, int day)
        {
            switch (day)
            {
                case 1:
                    return new Day012019(input).Part1();
                case 2:
                    return new Day022019(input).Part1();
                case 3:
                    return new Day032019(input).Part1();
                case 4:
                    return new Day042019(input).Part1();
                case 5:
                    return new Day052019(input).Part1();
                case 6:
                    return new Day062019(input).Part1();
                case 7:
                    return new Day072019(input).Part1();
                default:
                    return "Day not implemented yet!";
            }
        }

        private string Part2_Compute(List<string> input, int day)
        {
            switch (day)
            {
                case 1:
                    return new Day012019(input).Part2();
                case 2:
                    return new Day022019(input).Part2();
                case 3:
                    return new Day032019(input).Part2();
                case 4:
                    return new Day042019(input).Part2();
                case 5:
                    return new Day052019(input).Part2();
                case 6:
                    return new Day062019(input).Part2();
                case 7:
                    return new Day072019(input).Part2();
                default:
                    return "Day not implemented yet!";
            }
        }
    }
}